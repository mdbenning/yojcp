package server;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

import static server.TCPServer.ANSI_RESET;

public class MessageClass {

    public static String request(RequestType type, Language language, String color) throws Exception {
        Object obj = new JSONParser().parse(new FileReader("src/server/Messages.json"));
        JSONObject message = (JSONObject) obj;
        String out = color + message.get(langTypeToString(language) + "_" + type) + ANSI_RESET;
        System.out.println(langTypeToString(language) + "_" + type);
        return out;
    }

    public static Language langStringToType(String language) {
        switch (language) {
            case "FR":
                return MessageClass.Language.FR;
            case "EN:":
                return MessageClass.Language.EN;
            case "DE":
                return MessageClass.Language.DE;
            default:
                return MessageClass.Language.EN;
        }
    }

    public static String langTypeToString(Language language) {
        switch (language) {
            case DE:
                return "DE";
            case EN:
                return "EN";
            case FR:
                return "FR";
            default:
                return "EN";
        }
    }

    public enum RequestType {
        HELP, WHISPER, BAN, KICK, DISCONNECT, SWEAR, UNKNOWN, SET_LANG,
        WHISPER_SENT, WHISPER_ERROR, SPAM, LANG, INVALID_NAME, USERNAME_SET, USERNAME_GET, USER_JOINED, USER_LEFT, TAKEN_NAME,
        BORED, ANGRY, SAD, HAPPY, CONFUSED, SCARED, DOUBTFUL, STATUS_DOES_NOT_EXIST
    }

    public enum Language {
        DE, EN, FR
    }
}
